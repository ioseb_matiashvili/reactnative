import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  Modal,
  Button,
  Alert,
  PanResponder,
  Share,
} from "react-native";
import { Card, Icon, Rating, Input } from "react-native-elements";

import { connect } from "react-redux";
import { baseUrl } from "../shared/baseUrl";
import { postFavorite } from "../redux/ActionCreators";
import { postComment } from "../redux/ActionCreators";

import * as Animatable from "react-native-animatable";

const mapStateToProps = (state) => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    favorites: state.favorites,
  };
};

const mapDispatchToProps = (dispatch) => ({
  postFavorite: (dishId) => dispatch(postFavorite(dishId)),
  postComment: (dishId, rating, author, comment) =>
    dispatch(postComment(dishId, rating, author, comment)),
});

function RenderDish(props) {
  const dish = props.dish;

  handleViewRef = (ref) => (this.view = ref);

  const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
    if (dx < -200) {
      return true;
    } else {
      return false;
    }
  };

  const recognizeComment = ({ moveX, moveY, dx, dy }) => {
    if (dx > 200) {
      return true;
    } else {
      return false;
    }
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (e, gestureState) => {
      return true;
    },
    onPanResponderGrant: () => {
      this.view
        .rubberBand(1000)
        .then((endState) =>
          console.log(endState.finished ? "finished" : "cancelled")
        );
    },
    onPanResponderEnd: (e, gestureState) => {
      console.log("pan responder end", gestureState);
      if (recognizeDrag(gestureState))
        Alert.alert(
          "Add Favorite",
          "Are you sure you wish to add " + dish.name + " to favorite?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "OK",
              onPress: () => {
                props.favorite
                  ? console.log("Already favorite")
                  : props.onPress();
              },
            },
          ],
          { cancelable: false }
        );
      if (recognizeComment(gestureState)) props.toggleCommentModall();
      return true;
    },
  });

  const shareDish = (title, message, url) => {
    Share.share(
      {
        title: title,
        message: title + ": " + message + " " + url,
        url: url,
      },
      {
        dialogTitle: "Share " + title,
      }
    );
  };

  if (dish != null) {
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={2000}
        delay={1000}
        ref={this.handleViewRef}
        {...panResponder.panHandlers}
      >
        <Card feauteredTitle={dish.name} image={{ uri: baseUrl + dish.image }}>
          <Text style={{ margin: 10 }}>{dish.description}</Text>
          <View style={styles.row}>
            <Icon
              raised
              reverse
              name={props.favorite ? "heart" : "heart-o"}
              type="font-awesome"
              color="#f50"
              onPress={() =>
                props.favourite
                  ? console.log("Already favourite")
                  : props.onPress()
              }
            />
            <Icon
              raised
              reverse
              name="pencil"
              type="font-awesome"
              color="#512DA8"
              onPress={props.toggleCommentModall}
            />
            <Icon
              raised
              reverse
              name="share"
              type="font-awesome"
              color="#51D2A8"
              style={styles.cardItem}
              onPress={() =>
                shareDish(dish.name, dish.description, baseUrl + dish.image)
              }
            />
          </View>
        </Card>
      </Animatable.View>
    );
  } else {
    return <View></View>;
  }
}

function RenderComments(props) {
  const comments = props.comments;
  const renderCommentItem = ({ item, index }) => {
    //let starNumber = item.rating;

    return (
      <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
        <View key={index} style={{ margin: 10 }}>
          <Text style={{ fontSize: 14 }}>{item.comment}</Text>
          <View flex={1} flexDirection="row">
            <Text>{item.rating}</Text>
            <View style={styles.ratingIcon}>
              <Icon name="star" type="font-awesome" color="orange" size={20} />
            </View>
          </View>

          <Text style={{ fontSize: 12 }}>
            {"-- " + item.author + ", " + item.date}{" "}
          </Text>
        </View>
      </Animatable.View>
    );
  };

  return (
    <Card title="Comments">
      <FlatList
        data={comments}
        renderItem={renderCommentItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </Card>
  );
}

class Dishdetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      author: "",
      comment: "",
      showModal: false,
    };
  }

  toggleCommentModal = () => {
    this.setState({
      showModal: !this.state.showModal,
    });
  };

  handleCommentModal() {
    const { rating, author, comment } = this.state;
    const dishId = this.props.navigation.getParam("dishId", "");
    this.props.postComment(dishId, rating, author, comment);
    this.toggleCommentModal();
    console.log(JSON.stringify(this.state));
  }

  handleRating(rating) {
    this.setState({
      rating: rating,
    });
  }

  handleAuthor(author) {
    this.setState({
      author: author,
    });
  }

  handleComment(comment) {
    this.setState({
      comment: comment,
    });
  }

  resetForm() {
    this.setState({
      rating: 0,
      author: "",
      comment: "",
      showModal: false,
    });
  }

  markFavorite(dishId) {
    this.props.postFavorite(dishId);
  }

  static navigationOptions = {
    title: "Dish Details",
  };

  render() {
    const dishId = this.props.navigation.getParam("dishId", "");

    return (
      <ScrollView>
        <RenderDish
          dish={this.props.dishes.dishes[+dishId]}
          favorite={this.props.favorites.some((el) => el === dishId)}
          onPress={() => this.markFavorite(dishId)}
          toggleCommentModall={this.toggleCommentModal}
        />
        <RenderComments
          comments={this.props.comments.comments.filter(
            (comment) => comment.dishId === dishId
          )}
        />
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.showModal}
          onDismiss={() => {
            this.toggleCommentModal();
            this.resetForm();
          }}
          onRequestClose={() => {
            this.toggleCommentModal();
            this.resetForm();
          }}
        >
          <View>
            <Rating
              type="star"
              showRating
              startingValue={0}
              ratingCount={5}
              onFinishRating={(rating) => this.handleRating(rating)}
            />
            <View style={styles.input}>
              <Input
                onChangeText={(author) => this.handleAuthor(author)}
                placeholder="Author"
                leftIcon={{ type: "font-awesome", name: "user-o" }}
                style={styles.input}
              />
              <Input
                onChangeText={(comment) => this.handleComment(comment)}
                placeholder="Comment"
                leftIcon={{ type: "font-awesome", name: "comment-o" }}
                style={styles.input}
              />
            </View>

            <View style={styles.submitButton}>
              <Button
                style={styles.button}
                onPress={() => {
                  this.handleCommentModal();
                }}
                color="#512DA8"
                title="Submit"
              />
            </View>
            <View style={styles.cancelButton}>
              <Button
                style={styles.button}
                onPress={() => {
                  this.toggleCommentModal();
                  this.resetForm();
                }}
                color="#bfbfbf"
                title="Cancel"
              />
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    justifyContent: "center",
    flex: 1,
    flexDirection: "row",
  },
  input: {
    marginTop: 20,
  },
  submitButton: {
    marginTop: 35,
    alignContent: "center",
    marginLeft: "10%",
    width: "80%",
  },
  cancelButton: {
    marginTop: 35,
    alignContent: "center",
    marginLeft: "10%",
    width: "80%",
  },
  ratingIcon: {
    marginLeft: 5,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);
