// Dishes actions
export const DISHES_LOADING = "DISHES_LOADING";
export const ADD_DISHES = "ADD_DISHES";
export const DISHES_FAILED = "DISHES_FAILED";

// Comments actions
export const ADD_COMMENTS = "ADD_COMMENTS";
export const COMMENTS_FAILED = "COMMENTS_FAILED";

// Promotion actions
export const PROMOS_LOADING = "PROMOS_LOADING";
export const ADD_PROMOS = "ADD_PROMOS";
export const PROMOS_FAILED = "PROMOS_FAILED";

// Leaders actions
export const LEADERS_LOADING = "LEADERS_LOADING";
export const ADD_LEADERS = "ADD_LEADERS";
export const LEADERS_FAILED = "LEADERS_FAILED";

// Favorites actions
export const POST_FAVORITE = "POST_FAVORITE";
export const ADD_FAVORITE = "ADD_FAVORITE";
export const DELETE_FAVORITE = "DELETE_FAVORITE";

// Adding comments in modal
export const ADD_COMMENT = "ADD_COMMENT";
